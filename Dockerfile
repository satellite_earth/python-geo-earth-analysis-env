FROM continuumio/miniconda3

ADD geo_env.yml /tmp/geo_env.yml
RUN conda env create -f /tmp/geo_env.yml

# Pull the env name
RUN echo "source activate $(head -1 /tmp/geo_env.yml | cut -d' ' -f2)" > ~/.bashrc
ENV PATH /opt/conda/envs/$(head -1 /tmp/geo_env.yml | cut -d' ' -f2)/bin:$PATH
