python-geo-earth-analyses-env
-----------------------------

You will find a docker image with a conda environment that can be installed using a yaml file.


How to use:
-----------

### Install Geo Earth Analyses Conda Environment


Please, install git and conda for Python 3 (e.g. 3.6.5)

`conda env create -f geo_env.yml`

Linux: `source activate geo_env`

Windows: `activate geo_env`


### Docker Build


1. Install docker
2. Run the following code in order to build the docker:

`cd python-geo-earth-analyses-env`

`docker build -t narsimoes/python-geo-earth-analyses-env .`

`docker run -it narsimoes/python-geo-earth-analyses-env`
